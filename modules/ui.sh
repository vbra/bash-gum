# Funções para UI
draw_line() {
	local char line term_cols
	char=${1::1}
	term_cols=$(tput cols)
	printf -v line "%${term_cols}s"
	echo -n "${line// /${char:-─}}"
}

text_color() {
	text="$1"
	color="$2"
	echo "$(tput setaf "${color:-3}")${text}$(tput sgr0)"
}

list_colors() {
	for ((i = 0; i < 256; i++)); do
		echo -n '  '
		tput setab $i
		tput setaf $((((i > 231 && i < 244) || ((i < 17) && (i % 8 < 2)) || (\
		i > 16 && i < 232) && ((i - 16) % 6 < (i < 100 ? 3 : 2)) && ((i - 16) % 36 < 15)) ? 7 : 16))
		printf " C %03d " $i
		tput op
		((((i < 16 || i > 231) && ((i + 1) % 8 == 0)) || ((i > 16 && i < 232) && ((i - 15) % 6 == 0)))) &&
			printf "\n"
	done
}

process_options() {
	case $1 in
	"line")
		draw_line "$2"
		;;
	"color")
		text_color "$2" "$3"
		;;
	"list_colors")
		list_colors
		;;
	*)
		echo "Erro: Opção inválida ($1)."

		exit 1
		;;
	esac
}

process_options "$@"
