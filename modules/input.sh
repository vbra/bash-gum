# Funções para INPUT
position_cursor() {
	prompt_size=$((${#1} + 2))
	echo -ne "\033[${prompt_size}G"
}

show_placeholder() {
	echo -ne "\033[240m\033[30m $placeholder\033[0;0m "
}

process_options() {
	while getopts "h:p:t:" OPT; do
		case $OPT in
		h)
			header="${OPTARG}"
			;;
		p)
			placeholder="${OPTARG}"
			;;
		t)
			prompt="${OPTARG}"
			;;
		\?)
			echo "Erro: Opção inválida."
			exit
			;;
		esac
	done
}

draw_screen() {
	process_options "$@"
	[[ -n $header ]] && echo "$header"
	[[ -z $placeholder ]] && placeholder="Digite..."
	[[ -z $prompt ]] && prompt=">"
	echo -ne "$prompt"
	show_placeholder
	position_cursor "$prompt"
	read -r
	echo "$REPLY"
}

draw_screen "$@"
