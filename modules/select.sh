# Funções para SELECT
arguments_original=("$@")
arguments_modified=("$@")

item_selected="◉"
item_unselected="○"

# Função para exibir os argumentos com o prompt "[ ]" ou "[x]"
prepare_arguments() {
	for ((i = 0; i < "${#arguments_original[@]}"; i++)); do
		arguments_modified[i]="$item_unselected ${arguments_original[$i]}"
	done
}

display_arguments() {
	tput clear # Limpar a tela
	for ((i = 0; i < "${#arguments_modified[@]}"; i++)); do
		if [[ $i -eq $current_index ]]; then
			echo -e "\033[34m> ${arguments_modified[i]}\033[0;0m"
		else
			echo -e "  ${arguments_modified[i]}"
		fi
	done
}
# cursor invisible
tput civis

# Variável para armazenar o índice da linha selecionada pelo usuário
current_index=0

prepare_arguments
display_arguments

while true; do
	read -srn1
	case "$REPLY" in
	"A" | "k") # Seta para cima ou tecla "k"
		if ((current_index > 0)); then
			((current_index--))
			display_arguments
		fi
		;;
	"B" | "j") # Seta para baixo ou tecla "j"
		if ((current_index < ${#arguments_modified[@]} - 1)); then
			((current_index++))
			display_arguments
		fi
		;;
	" ") # Barra de espaço
		if [[ ! "${arguments_modified[current_index]}" =~ ^.*$item_selected.* ]]; then
			arguments_modified[current_index]="$item_selected ${arguments_original[$current_index]}"
		else
			arguments_modified[current_index]="$item_unselected ${arguments_original[$current_index]}"
		fi
		display_arguments
		;;
	"") # Tecla "enter"
		break
		;;
	esac
done

selected_lines=()
for ((i = 0; i < "${#arguments_modified[@]}"; i++)); do
	if [[ "${arguments_modified[i]}" =~ ^.*$item_selected.* ]]; then
		selected_lines+=("${arguments_original[i]}")
	fi
done

tput cnorm

printf "%s\n" "${selected_lines[@]}"
