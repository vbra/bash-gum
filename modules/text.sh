# Funções para TEXT
position_cursor() {
	prompt_size=$((${#1} + 2))
	echo -ne "\033[${prompt_size}G"
}

process_options() {
	while getopts "h:p:t:" OPT; do
		case $OPT in
		h)
			header="${OPTARG}"
			;;
		p)
			placeholder="${OPTARG}"
			;;
		t)
			prompt="${OPTARG}"
			;;
		\?)
			echo "Erro: Opção inválida."
			exit
			;;
		esac
	done
}

show_placeholder() {
	echo -ne "\033[240m\033[30m $placeholder\033[0;0m "
}

draw_screen() {
	process_options "$@"
	[[ -z $header ]] && header="Digite seu texto de entrada abaixo:"
	[[ -z $placeholder ]] && placeholder="Digite..."
	[[ -z $prompt ]] && prompt="┃"
	local help="Insira duas linhas em branco para concluir a inclusão do texto."

	echo "$header"
	echo -e "\033[240m\033[30m$help \033[0;0m "

	echo -ne "$prompt"
	show_placeholder
	position_cursor "$prompt"
	read -r input_text
	input_text+="\n"

	while IFS= read -r -p "$prompt " line; do
		[[ -z $line ]] && break
		input_text+="${line}\n"
	done

	echo -e "${input_text}"
}

draw_screen "$@"
