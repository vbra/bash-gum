# Funções para CONFIRM
draw_button_confirm() {
	echo -e "\033[44m\033[30m   $btn_confirm   \033[0;0m \033[240m\033[30m   $btn_reject   \033[0;0m "
}

draw_button_reject() {
	echo -e "\033[240m\033[30m   $btn_confirm   \033[0;0m \033[44m\033[30m   $btn_reject   \033[0;0m "
}

draw_screen() {
	clear
	echo "$header"
	[[ "$selected_button" = "confirm" ]] && draw_button_confirm || draw_button_reject
}

process_options() {
	while getopts "h:c:r:" OPT; do
		case $OPT in
		h)
			header="${OPTARG}"
			;;
		c)
			btn_confirm="${OPTARG}"
			;;
		r)
			btn_reject="${OPTARG}"
			;;
		\?)
			echo "Erro: Opção inválida."
			exit
			;;
		esac
	done
}

process_options_defaults() {
	[[ -n $header ]] && echo "$header"
	[[ -z $btn_confirm ]] && btn_confirm="Sim"
	[[ -z $btn_reject ]] && btn_reject="Não"
}

get_response() {
	# cursor invisible
	tput civis
	process_options "$@"
	process_options_defaults

	selected_button="confirm"
	draw_screen
	while true; do
		read -rsn1 key
		case "$key" in
		"A" | "D" | "h" | "k") # Seta para cima ou esquerda ou h ou k
			selected_button="confirm"
			draw_screen
			;;
		"B" | "C" | "j" | "l") # Seta para baixo ou direita ou j ou l
			selected_button="reject"
			draw_screen
			;;
		"") # Enter
			tput cnorm
			[[ "$selected_button" = "confirm" ]] && exit 0 || exit 1
			break
			;;
		esac
	done
}

get_response "$@"
