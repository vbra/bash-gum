# Funções para SPINNER
# Créditos: https://unix.stackexchange.com/a/565551

function shutdown() {
	tput cnorm # reset cursor
}
trap shutdown EXIT

function cursorBack() {
	echo -en "\033[$1D"
}

function spinner() {
	local LC_CTYPE=C

	local pid=$1
	local spin='⣾⣽⣻⢿⡿⣟⣯⣷'
	local charwidth=3

	# case $2 in
	# 0)
	# 	local spin='⠁⠂⠄⡀⢀⠠⠐⠈'
	# 	local charwidth=3
	# 	;;
	# 1)
	# 	local spin='-\|/'
	# 	local charwidth=1
	# 	;;
	# 2)
	# 	local spin="▁▂▃▄▅▆▇█▇▆▅▄▃▂▁"
	# 	local charwidth=3
	# 	;;
	# 3)
	# 	local spin="▉▊▋▌▍▎▏▎▍▌▋▊▉"
	# 	local charwidth=3
	# 	;;
	# esac

	local i=0

	# cursor invisible
	tput civis

	while kill -0 "$pid" 2>/dev/null; do
		local i=$(((i + charwidth) % ${#spin}))
		printf "%s" "${spin:$i:$charwidth}"

		cursorBack 1
		sleep .1
	done
	tput cnorm
	wait $pid # capture exit code
	return $?
}

("$@") &

spinner $!
