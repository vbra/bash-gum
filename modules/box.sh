# Funções para LABEL

process_options() {
	while getopts "l:s:c:t:" OPT; do
		case $OPT in
		l)
			label_text=" ${OPTARG} "
			;;
		s)
			box_size="${OPTARG}"
			;;
		c)
			box_color="${OPTARG}"
			;;
		t)
			content_text=" ${OPTARG} "
			;;
		\?)
			echo "Erro: Opção inválida."
			exit
			;;
		esac
	done
}

print_first_line() {
	tput setaf "$box_color"
	printf "┌─%s" "$label_text"
	printf '─%.0s' $(seq 1 $((box_size - ${#label_text} - 1)))
	printf "┐\n"

}

print_last_line() {
	tput setaf "$box_color"
	printf "└"
	printf '─%.0s' $(seq 1 $((box_size)))
	printf "┘\n"
	tput setaf 4
}

print_content_line() {
	local content_length=${#content_text}
	local content_length_text=$((box_size - content_length + 3))

	# Verificar se o conteúdo é maior que o tamanho da caixa
	if [ "$content_length" -gt "$box_size" ]; then
		local content_text_start="${content_text:0:5}"
		local content_text_end="${content_text: -$((box_size - 10))}"
		content_text="${content_text_start}...${content_text_end}"
		content_length_text=$((box_size - ${#content_text} + 3))
	fi

	printf "│$(tput setaf 4)%s$(tput setaf "$box_color")" "$content_text"
	printf "%*s\n" "$content_length_text" "│"
}

box_one_line() {
	print_first_line
	print_content_line "$content_text"
	print_last_line
}
process_options "$@"

[[ -z $content_text ]] && content_text=" $1 "
[[ -z $box_color ]] && box_color="4"
[[ -z $box_size ]] && box_size=${#content_text}

box_one_line
