# bash-gum

![](video.mp4)

Projeto descaradamente baseado no https://github.com/charmbracelet/gum

O intuito é recriar tais funções usando apenas o bash.

Cada uma dessas funções está inclusa no diretório "modules" e é chamada pelo script "bash-gum" seguido de um comando e suas opções.

## Comandos disponíveis:

- Input: recebe texto de entrada (uma única linha).

    Ex.: ``` bash-gum input -h "Informe seu nome" -p "Digite aqui..."```

- Text: recebe texto de entrada (múltiplas linhas).

    Ex.: ```bash-gum text -h "Nos conte uma bela história" -p "Digite algo..."```

- Select: permite usuário selecionar iten(s) da lista exibida.

    Ex.: ```bash-gum select "Uma opção muito boa" "Outra opção melhor" "Mais uma opção incrível"```

- Confirm: exibe dois botões para seleção de uma resposta afirmativa ou negativa.

    Ex.: ```bash-gum confirm -h "Ainda tem mais?" -c "Claro" -r "Por enquanto não"```

- Spinner: exibe padrão visual em movimento, enquanto programa informado nas opções não é concluído.

    Ex.: ```bash-gum spinner sleep 3```

- Box: exibe borda com título ao redor de texto informado.

    Ex.: ```bash-gum box -c 1 -l "Título" -s 50 -t "Conteúdo da label"```

- UI: exibe opções diversas para interface do usuário, tais como linhas e textos com cores.

    Ex.: ```bash-gum ui line```


## Próximos comandos a serem inclusos:

- Progress bar
- Painel (-h header, -b body, -f footer, -p principal background color, -s secondary background color)
- Menu
- Align (left, center, right)
