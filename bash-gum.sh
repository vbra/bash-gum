#!/usr/bin/env bash
# --------------------------------------------------
# Projeto   : bash-gum
# Arquivo   : bash-gum.sh
# Descrição :
# Versão    : 0.0.1.0
# Data      : 25/07/2023 - 14:02
# Autor     : Victor Araujo <victorbra@gmail.com>
# Licença   : GNU/GPL v3.0
# --------------------------------------------------
# Uso:
# Dependências :
# --------------------------------------------------
base_path="$(dirname "$(realpath "$0")")"
base_name="$(basename "$0")"
cmd="$1"

usage() {
	echo "$base_name <comando> <opções>

    - Comandos disponíveis:
        confirm
            - Opções para o comando confirm:
                -h <texto> : Texto para título (header).
                -c <texto> : Texto para botão selecionando confirmação.
                -r <texto> : Texto para botão selecionando rejeição.

        input
            - Opções para o comando input:
                -h <texto>  Texto para o título (header).
                -p <texto> : Texto para o placeholder.
                -t <texto> : Texto de prompt.
        text
            - Opções para o comando text:
                -h <texto> : Texto para o título (header).
                -p <texto> : Texto para o placeholder.
                -t <texto> : Texto de prompt.
        select
            - Cada opção informada corresponderá a um item da lista exibida. O usuário pode selecionar quantos itens quiser.

        spinner
            - A opção informada deve corresponder a um programa externo.

        box
            - Opções para o comando label:
                -l <texto> : Texto para o título (label).
                -c <número> : Cor para borda e título (v. ui list_colors).
                -t <texto> : Texto para conteúdo da caixa.
                -s <número> : Tamanho da caixa a envolver o texto. Se não informado caixa envolverá todo o texto.

        ui
            - Opções para o comando UI:
                line <argumento adicional> : Desenha uma linha em toda a largura da tela. Um argumento adicional pode ser informado para ser usado como caractere a compor a linha.
                color '<texto>' <cor> : Exibe texto em cores variadas. A cor deve ser informada por meio de números.
                list_colors : Exibe lista com números e cores que podem ser informadas nas demais opções."

}

main() {
	# Função principal
	case "$cmd" in
	"confirm")
		source "$base_path/modules/confirm.sh"
		get_confirm_response
		;;
	"input")
		source "$base_path/modules/input.sh"
		;;
	"text")
		source "$base_path/modules/text.sh"
		;;
	"select")
		source "$base_path/modules/select.sh"
		;;
	"spinner")
		source "$base_path/modules/spinner.sh"
		;;
	"ui")
		source "$base_path/modules/ui.sh"
		;;
	"box")
		source "$base_path/modules/box.sh"
		;;
	*)
		usage
		;;
	esac

}

# Execução --------------------------------------------------------------------
shift
main "$@"
